package 
{
	import flash.display3D.Context3DTextureFormat;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	import deng.fzip.FZip;
	import deng.fzip.FZipFile;
	
	import starling.events.EventDispatcher;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.AssetManager;
	
	public class MAssetsManager extends EventDispatcher
	{
	
	private var GameData:String;
	public var gameObject:*;
	private static var textureNames:Object;
	static public var assetManager:AssetManager;
	private static var gameTextureAtlas:TextureAtlas;
	private static var gameTextureAtlass:TextureAtlas;
	private static var mapTextureAtlas:TextureAtlas;
	private static var winTextureAtlas:TextureAtlas;
	private static var lifeBankTextureAtlas:TextureAtlas;
	private static var systemWindowTextureAtlas:TextureAtlas;
	private static var mapSystemWinTextureAtlas:TextureAtlas;
	private static var bonusLvlTextureAtlas:TextureAtlas;
	private var currentXML:XML;
	private var currentATF:Texture;
	private var __onLoadComplete:Function;
	private var zipMedia:FZip;
	private var zipMedia2:FZip;
	private var mediaGraphicsATFArray:Vector.<FZipFile> = new Vector.<FZipFile>();
	private var mediaGraphicsXMLArray:Vector.<FZipFile> = new Vector.<FZipFile>();
	
	static public const CAMPUS:String = "Campus";
	static public const ARISTA:String = "Arista";
	static public const CALIBRIB:String = "Calibrib";
	
	[Embed(source="../media/fonts/Arista.ttf", embedAsCFF='false', fontName='Arista', 
						fontFamily="MyFontName")]
	private static const Arista:Class;
	
	[Embed(source="../media/fonts/CAMPUS.ttf", embedAsCFF='false', fontName='Campus', 
						fontFamily="MyFontName")]
	private static const Campus:Class;
	
	[Embed(source="../media/fonts/calibrib.ttf", embedAsCFF='false', fontName='Calibrib', 
						fontFamily="MyFontName")]
	private static const Calibrib:Class;
	
		public function MAssetsManager(onLoadComplete:Function)
		{
			super();
			
			__onLoadComplete = onLoadComplete;
			assetManager = new AssetManager();
			
			assetManager.textureFormat = Context3DTextureFormat.COMPRESSED_ALPHA;
			
			var req2:URLRequest = new URLRequest("../media/graphics.zip");
			
			 zipMedia2 = new FZip();
			zipMedia2.addEventListener(Event.COMPLETE, loadCompleteMedia2);
			zipMedia2.load(req2);
		}
		
		protected function loadCompleteMedia2(event:Event):void
		{
			var len:int = zipMedia2.getFileCount();
			var fileMedia:FZipFile;
			var index:int;
			checkForATF();
			checkForXML();
			
			loadTextureByArray();
			function checkForATF():void
			{
				for (var i:int = 0; i < len; i++) 
				{
					fileMedia = zipMedia2.getFileAt(i);
					index = fileMedia.filename.indexOf(".atf")
					if( index != -1) 
					{
						mediaGraphicsATFArray.push(fileMedia);
					}
				}
			}
			
			function checkForXML():void
			{
				for (var i:int = 0; i < len; i++) 
				{
					fileMedia = zipMedia2.getFileAt(i);
					index = fileMedia.filename.indexOf(".xml")
					if( index != -1) mediaGraphicsXMLArray.push(fileMedia);
				}
			}
		}
		
		private function loadTextureByArray():void
		{
			
			for (var i:int = 0; i < mediaGraphicsATFArray.length; i++) 
			{
				var name:String = mediaGraphicsATFArray[i].filename.substring(0, mediaGraphicsATFArray[i].filename.length);
				var nameATF:String = name.slice( 0, name.length -4);
				currentATF = Texture.fromAtfData(mediaGraphicsATFArray[i].content as ByteArray, 0, false);
				assetManager.addTexture(nameATF, currentATF);
				var xml:XML = takeXML(nameATF);
				var atlas:TextureAtlas = new TextureAtlas(currentATF, xml);
				assetManager.addTextureAtlas(nameATF, atlas);
			}
			 function takeXML(nameATF:String):XML
			{
				 for (var j:int = 0; j < mediaGraphicsXMLArray.length; j++) 
				 {
					 var name:String = mediaGraphicsXMLArray[j].filename.substring(0, mediaGraphicsXMLArray[j].filename.length);
					 var nameXML:String = name.slice( 0, name.length -4);
					 if(nameATF == nameXML)
					 {
						 currentXML = XML(mediaGraphicsXMLArray[j].content);
						 assetManager.addXml(nameXML, currentXML);
						 return currentXML; 
					 }
				 }
				return null;
			}
			__onLoadComplete.call();
		}
		
		static public function addXML(id:String, data:Class):void
		{
			assetManager.addXml(id, XML(new  data));
		}
		
		static public function getTexture(id:String):Texture
		{
			return assetManager.getTexture(id);
		}
		
		static public function getXML(id:String):XML
		{
			return assetManager.getXml(id);
		}
	}
}