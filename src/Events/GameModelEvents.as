package Events
{

	public class GameModelEvents
	{
		public static var CHECK_PIECE:String = "CHECK_PIECE";
		public static var SCORE_UPDATE:String = "SCORE_UPDATE";
		public static var PLAYER_NO_MOVE:String = "PLAYER_NO_MOVE";
		public static var ENEMY_NO_MOVE:String = "ENEMY_NO_MOVE";
		
		public function GameModelEvents()
		{
			
		}
	}
}