package 
{
	import GameLogic.MapScreen;
	import GameLogic.GameHelpingService.ScreenManager;
	
	public class GameInit
	{
		static private var __init:GameInit		
		static public function get instance():GameInit
		{
			if(!__init) __init = new GameInit(new singletone);
			return __init;
		}
		
		public function GameInit(instance:singletone)
		{
		}
		
		public function start():void
		{
			ScreenManager.instance.show( MapScreen );
		}
	}
}
class singletone{}