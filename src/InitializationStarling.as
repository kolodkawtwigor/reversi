package
{
	import flash.display.Stage;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	
	import starling.core.Starling;
	import starling.events.EventDispatcher;

	public class InitializationStarling extends EventDispatcher
	{
		private var mStarling:Starling;
		
		static private var _init:InitializationStarling;
		static public function get instance():InitializationStarling { return _init; }
		public static var _stage:Stage;	
		
		public function InitializationStarling(stage:Stage)
		{
			_stage = stage;
			
			var stageSize:Rectangle  = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
			var screenSize:Rectangle = new Rectangle(0, 0, stage.fullScreenWidth, stage.fullScreenHeight);
			var viewPort:Rectangle = stageSize;
			
			Starling.handleLostContext = true;
			
			stage.addEventListener(flash.events.Event.RESIZE, onResize);
			mStarling = new Starling(MainApplication, stage, viewPort, null, "auto", "auto");
			mStarling.enableErrorChecking = Capabilities.isDebugger;
			mStarling.antiAliasing = 16;
			mStarling.start();
		}
		
		private function onResize(e:flash.events.Event):void
		{
			var viewRect:Rectangle = new Rectangle(0, 0, _stage.stageWidth, _stage.stageHeight);
			mStarling.viewPort = viewRect;
			mStarling.stage.stageWidth = _stage.stageWidth;
			mStarling.stage.stageHeight = _stage.stageHeight;
		}
	}
}