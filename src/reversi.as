package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageQuality;
	import flash.display.StageScaleMode;
	
	[SWF(width="760", height="700", frameRate="60", wmode="direct")]
	public class reversi extends Sprite
	{
		static private var _init:reversi;
		static public function instance():reversi { return _init; }
		
		public function reversi()
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.quality = StageQuality.BEST;
			
			var initializationStarling:InitializationStarling = new InitializationStarling(stage);
		}
	}
}