package GameLogic.Models
{
	import flash.geom.Point;
	
	import GameLogic.Controllers_$_Views.InGame.Piece;
	
	import starling.events.EventDispatcher;

	public class GameModel extends EventDispatcher
	{
		static public var INSTANCE:GameModel;
		public var grid_data:Vector.<Vector.<Piece>>;
		public  const ROW:int = Service.instance.configurationModel.boardRow;
		public  const COL:int = Service.instance.configurationModel.boardCol;
		public var PLAYER_COLOR:uint;
		public var ENEMY_COLOR:uint;
		
		
		
		public var playerMove:Boolean = true;
		public var complexity:String;
		public function GameModel()
		{
			createGameBoard();
			
		}
		
		private function createGameBoard():void
		{
			grid_data = new Vector.<Vector.<Piece>>();
			
			createBoardPiece();
			var piece:Piece;
			
			function createBoardPiece():void
			{
				for (var i:int = 0; i < COL; i++) 
				{
					grid_data.push( new Vector.<Piece>());
					for (var j:int = 0; j < ROW; j++)
					{
						piece = new Piece();
						piece.pos_grid = new Point(i,j);
						grid_data[i].push( piece );
					}
				}
			}
		}
		
		public function initModel():void
		{
			INSTANCE = this;
		}
	}
}