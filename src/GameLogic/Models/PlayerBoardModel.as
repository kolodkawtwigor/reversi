package GameLogic.Models
{
	import Events.GameModelEvents;
	
	import GameLogic.Controllers_$_Views.InGame.GameStatPanel;
	import GameLogic.Controllers_$_Views.InGame.InfoGame;
	import GameLogic.Controllers_$_Views.InGame.Piece;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;

	public class PlayerBoardModel extends EventDispatcher
	{
		public var grid_data:Vector.<Vector.<Piece>>;
		static public var INSTANCE:PlayerBoardModel;
		public  const ROW:int = Service.instance.configurationModel.boardRow;
		public  const COL:int = Service.instance.configurationModel.boardCol;
		private var __gameModel:GameModel;
		private var allConquerPossibles:Vector.<Vector.<Piece>> = new Vector.<Vector.<Piece>>();
		public function PlayerBoardModel()
		{
			__gameModel = GameModel.INSTANCE;
			this.grid_data = __gameModel.grid_data;
		}
		
		public function initModel():void
		{
			INSTANCE = this;
		}
		
		public function checkPiece(piece:Piece):void
		{
			foundConquerPiece(piece);
			if(allConquerPossibles.length != 0 )drawConquerPiece();
		}
		
		public function checkPlayerMove():void
		{
			allConquerPossibles = new Vector.<Vector.<Piece>>();
			var piece:Piece;
			for (var i:int = 0; i < grid_data.length; i++) 
			{
				for (var j:int = 0; j < grid_data[i].length; j++) 
				{
					piece = grid_data[i][j];
					if (!piece.active ) foundConquerPiece(piece);
				}
			}
			var playerPossible:Vector.<Piece> = new Vector.<Piece>();
			for (var k:int = 0; k < allConquerPossibles.length; k++) 
			{
				for (var z:int = 0; z < allConquerPossibles[k].length; z++) 
				{
					playerPossible.push(allConquerPossibles[k][z]);
				}
			}
			if(playerPossible.length == 0 ) 
			{
//				GameModel.INSTANCE.playerMove = false;
				dispatchEvent( new Event(GameModelEvents.SCORE_UPDATE));
				dispatchEvent(new Event(GameModelEvents.PLAYER_NO_MOVE));
				EnemyBoardModel.INSTANCE.enemyMove();
			}
			else
			{
				InfoGame.instance().__playerNoMove = false;
			}
			
			allConquerPossibles = new Vector.<Vector.<Piece>>();
		}
		
		private function drawConquerPiece():void
		{
			for (var k:int = 1; k < allConquerPossibles[0].length; k++) 
			{
				allConquerPossibles[0][k].deActivateEnemy();
			}
			
			GameModel.INSTANCE.playerMove = false;
			allConquerPossibles[0][0].activatePlayer();
			dispatchEvent( new Event(GameModelEvents.SCORE_UPDATE));
			
			if(!GameStatPanel.instance().gameEnd) EnemyBoardModel.INSTANCE.enemyMove();
			
			allConquerPossibles = new Vector.<Vector.<Piece>>();
		}
		
		public function foundConquerPiece(piece:Piece):void
		{
			var samePiecesInPath:Vector.<Piece> = new Vector.<Piece>();
			var enemyPiecesInPath:Vector.<Piece> = new Vector.<Piece>();
			var allEnemyPieces:Vector.<Vector.<Piece>> = new Vector.<Vector.<Piece>>();
			
			var xPiece:uint = piece.pos_grid.x;
			var yPiece:uint = piece.pos_grid.y;
			
			var xCheckPiece:Number;
			var yCheckPiece:Number;
			var checkPiece:Piece;
			
			checkYPathToUp();
			checkYPathToBotton();
			
			checkXPathToRight();
			checkXPathToLeft();
			
			checkYXPathToUpRight();
			checkYXPathToBottonRight();
			
			checkYXPathToBottonLeft();
			checkYXPathToUpLeft();
			
			if(allEnemyPieces.length != 0 ) checkEnemyPiece();
			
			function checkEnemyPiece():void
			{
				var vec:Vector.<Piece> = new Vector.<Piece>();
				vec.push(piece);
				
				for (var j:int = 0; j < allEnemyPieces.length; j++) 
				{
					for (var i:int = 0; i < allEnemyPieces[j].length; i++) 
					{
						vec.push(allEnemyPieces[j][i]);
					}
				}
				
				allConquerPossibles.push(vec);
			}
			
			function checkYXPathToUpLeft():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece - i;
					yCheckPiece = yPiece - k;
					
					if( xCheckPiece < 0  || yCheckPiece < 0) break;
					
					checkPiece = grid_data[xCheckPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.enemyPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.playerPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.playerPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						break;
					}
				}
			}
			
			function checkYXPathToBottonLeft():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece - i;
					yCheckPiece = yPiece + k;
					
					if( xCheckPiece < 0  || yCheckPiece >= ROW) break;
					
					checkPiece = grid_data[xCheckPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.enemyPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.playerPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.playerPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						break;
					}
				}
			}
			function checkYXPathToBottonRight():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece + i;
					yCheckPiece = yPiece + k;
					
					if( xCheckPiece >= COL  || yCheckPiece >= ROW) break;
					
					checkPiece = grid_data[xCheckPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.enemyPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.playerPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.playerPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						break;
					}
				}
			}
			
			function checkXPathToLeft():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece - i;
					
					if( xCheckPiece < 0  ) break;
					checkPiece = grid_data[xCheckPiece][yPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.enemyPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.playerPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.playerPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						break;
					}
				}
			}
			
			function checkYPathToBotton():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1; i < 100; i++) 
				{
					yCheckPiece = yPiece + i;
					
					if( yCheckPiece < 0 || yCheckPiece >= ROW ) break;
					
					checkPiece = grid_data[xPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.enemyPiece) enemyPiecesInPath.push(checkPiece);
					
					if(  checkPiece.playerPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.playerPiece && enemyPiecesInPath.length != 0 ) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						break;
					}
				}
			}
			
			function checkXPathToRight():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece + i;
					
					if( xCheckPiece >= COL  ) break;
					checkPiece = grid_data[xCheckPiece][yPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.enemyPiece) 	enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.playerPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.playerPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						break;
					}
				}
			}
			
			function checkYXPathToUpRight():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece + i;
					yCheckPiece = yPiece - k;
					
					if( xCheckPiece >= COL  || yCheckPiece < 0) break;
					checkPiece = grid_data[xCheckPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.enemyPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.playerPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.playerPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						break;
					}
				}
			}
			
			function checkYPathToUp():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1; i < 100; i++) 
				{
					yCheckPiece = yPiece - i;
					
					if( yCheckPiece < 0 || yCheckPiece >= ROW ) break;
					
					checkPiece = grid_data[xPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.enemyPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.playerPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.playerPiece && enemyPiecesInPath.length != 0 ) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						break;
					}
				}
			}
		}
	}
}