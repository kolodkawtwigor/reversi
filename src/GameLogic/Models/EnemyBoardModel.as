package GameLogic.Models
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import Events.GameModelEvents;
	
	import GameLogic.Controllers_$_Views.InGame.GameStatPanel;
	import GameLogic.Controllers_$_Views.InGame.InfoGame;
	import GameLogic.Controllers_$_Views.InGame.Piece;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;

	public class EnemyBoardModel extends EventDispatcher
	{
		public var grid_data:Vector.<Vector.<Piece>>;
		static public var INSTANCE:EnemyBoardModel;
		public  const ROW:int = Service.instance.configurationModel.boardRow;
		public  const COL:int = Service.instance.configurationModel.boardCol;
		private var __gameModel:GameModel;
		private var allConquerPossibles:Vector.<Vector.<Piece>> = new Vector.<Vector.<Piece>>();
		private var timer:Timer;
		
		public function EnemyBoardModel()
		{
			__gameModel = GameModel.INSTANCE;
			this.grid_data = __gameModel.grid_data;
		}
		
		public function initModel():void
		{
			INSTANCE = this;
		}
		
		public function enemyMove():void
		{
			StatusStage.deactive();
			timer = new Timer(500);
			timer.addEventListener(TimerEvent.TIMER, move);
			timer.start();
		}
		
		protected function move(event:TimerEvent):void
		{
			timer.stop();
			foundAllPossibles();
			if(allConquerPossibles.length!= 0) 
			{
				InfoGame.instance().__enemyNoMove = false;
				conquerEnemyPiece();
			}
			else 
			{
				GameModel.INSTANCE.playerMove = true;
				dispatchEvent( new Event(GameModelEvents.SCORE_UPDATE));
				dispatchEvent(new Event(GameModelEvents.ENEMY_NO_MOVE));
				trace( "нет хода- враг")
			}
			
			//			проверка на возможный ход игрока
			if(!GameStatPanel.instance().gameEnd) PlayerBoardModel.INSTANCE.checkPlayerMove();
			
			StatusStage.active();
		}
		
		private function foundAllPossibles():void
		{
			allConquerPossibles = new Vector.<Vector.<Piece>>();
			var piece:Piece;
			for (var i:int = 0; i < grid_data.length; i++) 
			{
				for (var j:int = 0; j < grid_data[i].length; j++) 
				{
					piece = grid_data[i][j];
					if (!piece.active ) checkPiece(piece);
				}
			}
		}
		
		private function conquerEnemyPiece():void
		{
			
			
			
			
			
			
			
			
			
			
			
			
			
			var arr:Array = new Array();
			
			for (var j:int = 0; j < allConquerPossibles.length; j++) 
			{
				arr.push( {len:allConquerPossibles[j].length , num:j});
			}
			
			arr.sortOn("len" , Array.NUMERIC);
			arr.reverse();
			
			var rand:uint;
			var len:uint
			switch(GameModel.INSTANCE.complexity)
			{
				case "HARD":
				{
					rand = arr[0].num
					break;
				}
				case "MID":
				{
					len = Math.floor(arr.length>>1);
					rand = arr[len].num
					break;
				}
				case "EASY":
				{
					len = arr.length - 1;
					rand = arr[len].num
					break;
				}
					
				default:
				{
					break;
				}
			}
			
			for (var k:int = 1; k < allConquerPossibles[rand].length; k++) 
			{
				allConquerPossibles[rand][k].deActivatePlayer();
			}
			
			GameModel.INSTANCE.playerMove = true;
			allConquerPossibles[rand][0].activateEnemy();
			dispatchEvent( new Event(GameModelEvents.SCORE_UPDATE));
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		}
		
		
		
		public function checkPiece(piece:Piece):void
		{
			var samePiecesInPath:Vector.<Piece> = new Vector.<Piece>();
			var enemyPiecesInPath:Vector.<Piece> = new Vector.<Piece>();
			var allEnemyPieces:Vector.<Vector.<Piece>> = new Vector.<Vector.<Piece>>();
			
			var xPiece:uint = piece.pos_grid.x;
			var yPiece:uint = piece.pos_grid.y;
			
			var xCheckPiece:Number;
			var yCheckPiece:Number;
			var checkPiece:Piece;
			
			
			checkYPathToUp();
			checkYPathToBotton();
			
			checkXPathToRight();
			checkXPathToLeft();
			
			checkYXPathToUpRight();
			checkYXPathToBottonRight();
			
			checkYXPathToBottonLeft();
			checkYXPathToUpLeft();
			
			if(allEnemyPieces.length != 0 ) checkEnemyPiece();
			 
			function checkEnemyPiece():void
			{
				var vec:Vector.<Piece> = new Vector.<Piece>();
				vec.push(piece);
				
				for (var j:int = 0; j < allEnemyPieces.length; j++) 
				{
					for (var i:int = 0; i < allEnemyPieces[j].length; i++) 
					{
						vec.push(allEnemyPieces[j][i]);
					}
				}
				
				allConquerPossibles.push(vec);
			}
			
			function checkYXPathToUpLeft():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece - i;
					yCheckPiece = yPiece - k;
					
					if( xCheckPiece < 0  || yCheckPiece < 0) break;
					
					checkPiece = grid_data[xCheckPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.playerPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						enemyPiecesInPath =  new Vector.<Piece>();
						break;
					}
				}
			}
			
			function checkYXPathToBottonLeft():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece - i;
					yCheckPiece = yPiece + k;
					
					if( xCheckPiece < 0  || yCheckPiece >= ROW) break;
					
					checkPiece = grid_data[xCheckPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.playerPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						enemyPiecesInPath =  new Vector.<Piece>();
						break;
					}
				}
			}
			function checkYXPathToBottonRight():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece + i;
					yCheckPiece = yPiece + k;
					
					if( xCheckPiece >= COL  || yCheckPiece >= ROW) break;
					
					checkPiece = grid_data[xCheckPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.playerPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						enemyPiecesInPath =  new Vector.<Piece>();
						break;
					}
				}
			}
			
			function checkXPathToLeft():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece - i;
					
					if( xCheckPiece < 0  ) break;
					checkPiece = grid_data[xCheckPiece][yPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.playerPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						enemyPiecesInPath =  new Vector.<Piece>();
						break;
					}
				}
			}
			
			function checkYPathToBotton():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1; i < 100; i++) 
				{
					yCheckPiece = yPiece + i;
					
					if( yCheckPiece < 0 || yCheckPiece >= ROW ) break;
					
					checkPiece = grid_data[xPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.playerPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length != 0 ) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						enemyPiecesInPath =  new Vector.<Piece>();
						break;
					}
				}
			}
			
			function checkXPathToRight():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece + i;
					
					if( xCheckPiece >= COL  ) break;
					checkPiece = grid_data[xCheckPiece][yPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.playerPiece) 	enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						enemyPiecesInPath =  new Vector.<Piece>();
						break;
					}
				}
			}
			
			function checkYXPathToUpRight():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1,  k:int = 1; k < 100; i++,k++) 
				{
					xCheckPiece = xPiece + i;
					yCheckPiece = yPiece - k;
					
					if( xCheckPiece >= COL  || yCheckPiece < 0) break;
					checkPiece = grid_data[xCheckPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.playerPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length != 0) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						enemyPiecesInPath =  new Vector.<Piece>();
						break;
					}
				}
			}
			
			function checkYPathToUp():void
			{
				enemyPiecesInPath =  new Vector.<Piece>();
				for (var i:int = 1; i < 100; i++) 
				{
					yCheckPiece = yPiece - i;
					
					if( yCheckPiece < 0 || yCheckPiece >= ROW ) break;
					
					checkPiece = grid_data[xPiece][yCheckPiece];
					if( !checkPiece.active) break;
					if(  checkPiece.playerPiece) enemyPiecesInPath.push(checkPiece);
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length == 0 ) break;
					if(  checkPiece.enemyPiece && enemyPiecesInPath.length != 0 ) 
					{
						allEnemyPieces.push(enemyPiecesInPath);
						enemyPiecesInPath =  new Vector.<Piece>();
						break;
					}
				}
			}
		}
		
		
	}
}