package GameLogic
{
	import Windows.ChooseSide;
	import GameLogic.Controllers_$_Views.InGame.GameStatPanel;
	import GameLogic.Controllers_$_Views.InGame.GameView;
	import GameLogic.Controllers_$_Views.InGame.TopGamePanel;
	import GameLogic.GameHelpingService.WindowsManager;
	import GameLogic.Models.EnemyBoardModel;
	import GameLogic.Models.GameModel;
	import GameLogic.Models.PlayerBoardModel;
	
	import starling.display.Canvas;
	import starling.display.Sprite;
	import starling.events.Event;
	import GameLogic.Controllers_$_Views.InGame.InfoGame;

	public class GameScreen extends Sprite
	{
		static private var _init:GameScreen;
		private var _gameView:GameView;
		private var _gameModel:GameModel;
		private var __playerBoardModel:PlayerBoardModel;
		private var __enemyBoardModel:EnemyBoardModel;
		private var __gameStatPanel:GameStatPanel;
		private var __topGamePanel:TopGamePanel;
		private var __backGround:Canvas;
		public   const  BACKGROUND_COLOR:uint = Service.instance.configurationModel.backgroundColor;
		private var __infoGame:InfoGame;
		static public function get instance():GameScreen { return _init; }
		
		public function GameScreen()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			_init = this;
			
			_gameModel = new GameModel();
			_gameModel.initModel();
			
			__playerBoardModel = new PlayerBoardModel();
			__playerBoardModel.initModel();
			__enemyBoardModel = new EnemyBoardModel();
			__enemyBoardModel.initModel();
				
			createBackGround();
			
			__gameStatPanel = new GameStatPanel();
			addChild(__gameStatPanel);
			_gameView = new GameView();
			addChild(_gameView);
			
			__topGamePanel = new TopGamePanel();
			addChild(__topGamePanel);
			
			__infoGame = new InfoGame();
			addChild(__infoGame);
			
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function createBackGround():void
		{
			__backGround = new Canvas();
			__backGround.beginFill(BACKGROUND_COLOR);
			__backGround.drawRectangle(0,0,MainApplication.instance.stage.stageWidth,MainApplication.instance.stage.stageHeight);
			addChild(__backGround);
		}
		
		private function onResize(e:Event = null):void
		{
			__backGround.width  = MainApplication.instance.stage.stageWidth;
			__backGround.height = MainApplication.instance.stage.stageHeight;
		}
		
		private function chooseWindow():void
		{
			WindowsManager.show( ChooseSide );
		}
		
		private function destroy(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}