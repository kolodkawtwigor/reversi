package GameLogic.GameHelpingService
{
	import flash.display.Loader;
	import flash.events.AsyncErrorEvent;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	public class ImageLoader extends Sprite
	{
		private var __url:String;
		private var __target:Sprite;
		private var __height:Number;
		public  var picture:Loader = new Loader();
		private var __width:Number;
		
		public function ImageLoader(url:String, target:Sprite, height:Number = 55, width:Number = 55)
		{
			__url = url;
			__target = target;
			__height = height;
			__width = width;
			load();
		}
		
		private function load():void
		{
			var pictureURL:String = __url; 
			var pictureURLReq:URLRequest = new URLRequest(pictureURL);
			
			var context:LoaderContext = new LoaderContext(); 
			context.checkPolicyFile = true;
			
			picture.load(pictureURLReq, context); 
			
//			picture.addEventListener(AsyncErrorEvent.ASYNC_ERROR, errorHandlerAsyncErrorEvent);
//			picture.addEventListener(IOErrorEvent.IO_ERROR, errorHandlerIOErrorEvent);
//			picture.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandlerSecurityErrorEvent);
//			picture.contentLoaderInfo.addEventListener(Event.INIT, initHandler);
//			picture.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, infoIOErrorEvent);
//			picture.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, progressListener)
			picture.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			
			
			
			
			
			
			
			function progressListener (e:ProgressEvent):void{
				trace("Downloaded " + e.bytesLoaded + " out of " + e.bytesTotal + " bytes");
			}
			function initHandler( e:Event ):void{
				trace( 'load init' );
			}
			function errorHandlerErrorEvent( e:ErrorEvent ):void{
				trace( 'errorHandlerErrorEvent ' + e.toString() );
			}
			function infoIOErrorEvent( e:IOErrorEvent ):void{
				trace( 'infoIOErrorEvent ' + e.toString() );
			}
			function errorHandlerIOErrorEvent( e:IOErrorEvent ):void{
				trace( 'errorHandlerIOErrorEvent ' + e.toString() );
			}
			function errorHandlerAsyncErrorEvent( e:AsyncErrorEvent ) :void{
				trace( 'errorHandlerAsyncErrorEvent ' + e.toString() );
			}
			function errorHandlerSecurityErrorEvent( e:SecurityErrorEvent ):void{
				trace( 'errorHandlerSecurityErrorEvent ' + e.toString(
				) );
			}
		}
		
		private function onComplete(event:Event):void 
		{	
//			try
//			{
				var pic:Texture = Texture.fromBitmap(event.currentTarget.content);
				var img:Image = new Image ( pic );
				img.name = "loaderImage"
				img.height = __height;
				img.width = __width;
//				img.scaleX = img.scaleY;
				__target.addChild(img);
				picture.contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete);
//			} 
//			catch(error:Error) 
//			{
//				var pictureURL:String = "http://static1.greemlins.com/word-master/icons/icon_50х50.png"; 
//				var pictureURLReq:URLRequest = new URLRequest(pictureURL);
//				picture.load(pictureURLReq); 
//			}
			
		}
	}
}