package GameLogic.GameHelpingService
{
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	
	public class MImage extends Image
	{
		private var _textureInstance:Texture;
		
		public function MImage(textureInstance:Texture )
		{
			super(textureInstance);
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			this.smoothing = TextureSmoothing.BILINEAR;
		}	
		
		private function destroy(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			
			this.texture.dispose();
			this.dispose();
		}
	}
}