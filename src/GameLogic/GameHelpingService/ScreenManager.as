package GameLogic.GameHelpingService
{
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class ScreenManager extends Sprite
	{
		public var currentScreen:Sprite;
		private var _screenClass:Class;
		
		static private var _init:ScreenManager;
		static public function get instance():ScreenManager
		{
			if(!_init) _init = new ScreenManager();
			return _init;
		}
		
		public function ScreenManager()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		public function show(screenClass:Class):void
		{
			if(currentScreen)
			{
				MainApplication.instance.removeChild(currentScreen);
//				currentScreen.dispose();
			}
			currentScreen = new screenClass();
			_screenClass = screenClass;
			MainApplication.instance.addChild(currentScreen);
		}
	}
}