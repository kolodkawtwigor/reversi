package GameLogic.Controllers_$_Views.InGame
{
	
	import flash.text.Font;
	import flash.text.TextFormatAlign;
	
	import Events.GameModelEvents;
	
	import GameLogic.MapScreen;
	import GameLogic.GameHelpingService.MImage;
	import GameLogic.GameHelpingService.WindowsManager;
	import GameLogic.Models.EnemyBoardModel;
	import GameLogic.Models.GameModel;
	import GameLogic.Models.PlayerBoardModel;
	
	import Windows.Rule;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import Windows.GameEnd;
	
	public class GameStatPanel extends Sprite
	{
		static private var _init:GameStatPanel;
		static public function instance():GameStatPanel { return _init; }
		private var __playerScoreTF:TextField;
		private var __enemyScoreTF:TextField;
		private var __playerScore:int = 0;
		private var __enemyScore:int = 0;
		private var __playerBoardModel:PlayerBoardModel;
		private var __enemyBoardModel:EnemyBoardModel;
		private var _gameModel:GameModel;
		private var grid_data:Vector.<Vector.<Piece>>;
		public var gameEnd:Boolean = true;
		private var __playerScoreBG:MImage;
		public  const  WHITE_COLOR:String = MapScreen.PLAYER_COLOR;
		public  const  BLACK_COLOR:String = MapScreen.ENEMY_COLOR;
		private var __enemyScoreBG:MImage;
		public var gameResult:String;
		public function GameStatPanel()
		{
			__playerBoardModel = PlayerBoardModel.INSTANCE;
			__enemyBoardModel = EnemyBoardModel.INSTANCE;
			_init = this;
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
			
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			_gameModel = GameModel.INSTANCE;
			grid_data = _gameModel.grid_data;
			createTF();
			addListeners();
			onResize();
		}
		
		private function addListeners():void
		{
			__enemyBoardModel.addEventListener(GameModelEvents.SCORE_UPDATE , updateScore);
			__playerBoardModel.addEventListener(GameModelEvents.SCORE_UPDATE , updateScore);
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function onResize(e:Event = null):void
		{
			this.x = 50;
			this.y = 0;
		}
		
		private function updateScore(e:Event):void
		{
			var piece:Piece;
			__playerScore = 0;
			__enemyScore = 0
			for (var i:int = 0; i < grid_data.length; i++) 
			{
				for (var j:int = 0; j < grid_data[i].length; j++) 
				{
					piece = grid_data[i][j];
					if(piece.playerPiece) __playerScore ++;
					else if(piece.enemyPiece) __enemyScore++ ;
				}
			}
			__playerScoreTF.text = __playerScore.toString();
			__enemyScoreTF.text = __enemyScore.toString();
			
//			проверка на завершённость игры
			gameEnd = true;
			circ :for (var k:int = 0; k < grid_data.length; k++) 
			{
				for (var z:int = 0; z < grid_data[k].length; z++) 
				{
					piece = grid_data[k][z];
					if(!piece.active) 
					{
						gameEnd = false;
						break circ;
					}
				}
			}
			if(gameEnd) 
			{
				trace("gameEnd")
				if(__playerScore >  __enemyScore)  gameResult = "playerWin"
				else if( __playerScore <  __enemyScore)  gameResult = "enemyWin"
				else gameResult = "draw"
				
				WindowsManager.show( GameEnd );
				
			}
		}	
		
		public function endGame():void
		{
			if(__playerScore >  __enemyScore)  gameResult = "noMoveWinPlayer"
			else if( __playerScore <  __enemyScore)  gameResult = "noMoveWinEnemy"
			else gameResult = "noMoveDraw"
		}
		
		private function createTF():void
		{
			var fonts:Array = Font.enumerateFonts(true);
			__playerScoreTF = new TextField(40, 40, __playerScore.toString(),  MAssetsManager.ARISTA, 27, 0x362F27);
			addChild(__playerScoreTF); 
			__playerScoreTF.y = 10; 
			__playerScoreTF.x = 570; 
			__playerScoreTF.touchable = false; 
			__playerScoreTF.hAlign = TextFormatAlign.CENTER; 
			__playerScoreTF.batchable = true;
			
			var __ove:TextField = new TextField(40, 40, ":",  MAssetsManager.ARISTA, 35, 0x362F27);
			addChild(__ove);
			__ove.y = 10; 
			__ove.x = 595; 
			__ove.touchable = false; 
			__ove.hAlign = TextFormatAlign.CENTER; 
			__ove.batchable = true;
			
			__enemyScoreTF = new TextField(40, 40, "0",  MAssetsManager.ARISTA, 27, 0x362F27);
			addChild(__enemyScoreTF); 
			__enemyScoreTF.y = 10; 
			__enemyScoreTF.x = 620; 
			__enemyScoreTF.touchable = false; 
			__enemyScoreTF.hAlign = TextFormatAlign.CENTER;  
			__enemyScoreTF.batchable = true;
			
			var texture:Texture = MAssetsManager.assetManager.getTexture(WHITE_COLOR);
			__playerScoreBG = new MImage(texture); 
			__playerScoreBG.x = __playerScoreTF.x - 1;
			__playerScoreBG.y = __playerScoreTF.y - 2;
			__playerScoreBG.width = 45;
			__playerScoreBG.height = 45;
			addChild(__playerScoreBG);
			__playerScoreBG.parent.setChildIndex(__playerScoreBG, 0);
			
			texture = MAssetsManager.assetManager.getTexture(BLACK_COLOR);
			__enemyScoreBG = new MImage(texture); 
			__enemyScoreBG.x = __enemyScoreTF.x - 1;
			__enemyScoreBG.y = __enemyScoreTF.y - 2;
			__enemyScoreBG.width = 45;
			__enemyScoreBG.height = 45;
			addChild(__enemyScoreBG);
			__enemyScoreBG.parent.setChildIndex(__enemyScoreBG, 0);
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			__enemyBoardModel.removeEventListener(GameModelEvents.SCORE_UPDATE , updateScore);
			__playerBoardModel.removeEventListener(GameModelEvents.SCORE_UPDATE , updateScore);
			stage.removeEventListener(Event.RESIZE, onResize);
		}
	}
}