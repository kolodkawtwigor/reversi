package GameLogic.Controllers_$_Views.InGame
{
	
	import com.greensock.TweenMax;
	
	import GameLogic.GameScreen;
	import GameLogic.MapScreen;
	import GameLogic.GameHelpingService.ScreenManager;
	
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class TopGamePanel extends Sprite
	{
		private var __outplay:Button;
		private var __toMap:Button;
		public function TopGamePanel()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createSupportButtons();
			animateButtons();
			addListeners();
		}
		
		private function animateButtons():void
		{
			TweenMax.to(__outplay, 1.5, { y:50});
			TweenMax.to(__toMap, 1.5, { y:50});
		}
		
		private function addListeners():void
		{
			__outplay.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
			__toMap.addEventListener(TouchEvent.TOUCH , onGameKeyClickToMap);
		}
		
		private function onResize():void
		{
			
		}
		private function createSupportButtons():void
		{
			__outplay = new Button(MAssetsManager.assetManager.getTexture("reGame_button"))
			__outplay.scaleWhenDown = .9;
			addChild(__outplay);
			__outplay.alignPivot();
			__toMap = new Button(MAssetsManager.assetManager.getTexture("toMap_main_BTN"))
			__toMap.scaleWhenDown = .9;
			addChild(__toMap);
			__toMap.alignPivot();
			
			__outplay.x = 150;
			__outplay.y = -150;
			__toMap.x = 430;
			__toMap.y = -150;
		}
		
		protected function onGameKeyClickToMap(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED) ScreenManager.instance.show( MapScreen );
			}
		}
		
		protected function onGameKeyClick(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED) ScreenManager.instance.show( GameScreen );
			}
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			stage.removeEventListener(Event.RESIZE, onResize);
		}
	}
}