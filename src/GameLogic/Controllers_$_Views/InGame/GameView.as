package GameLogic.Controllers_$_Views.InGame
{
	import GameLogic.GameHelpingService.MImage;
	import GameLogic.Models.GameModel;
	
	import starling.display.Canvas;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;

	public class GameView extends Sprite
	{
		static private var _init:GameView;
		public  const RANGE:int = Service.instance.configurationModel.pieceRange;
		private var _gameModel:GameModel;
		private var grid_data:Vector.<Vector.<Piece>>;
		private var __outplayTF:TextField;
		private var __backGround:Canvas;
		private var background:MImage;
		
		static public function instance():GameView { return _init; }
		public function GameView()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			_init = this;
			removeEventListener(Event.ADDED_TO_STAGE, init);
			_gameModel = GameModel.INSTANCE;
			grid_data = _gameModel.grid_data;
			
			
			addBoardToStage();
			firstParameters();
			createBoardBG();
			addListeners();
			
			onResize();
			this.alignPivot();
			StatusStage.active();
//			console();
		}
		
		private function createBoardBG():void
		{
			var texture:Texture = MAssetsManager.assetManager.getTexture("main_bg_letter_substrate");
			background = new MImage(texture); 
			background.width = this.width + 25;
			background.height = this.height + 25;
			addChild(background);
			background.parent.setChildIndex(background, parent.numChildren -3);
		}
		
		private function firstParameters():void
		{
			var row:uint = _gameModel.COL >> 1;
			var col:uint = _gameModel.ROW >> 1;
			
			grid_data[row - 1][col - 1].activatePlayer();
			grid_data[row ][col].activatePlayer();
			grid_data[row - 1][col].activateEnemy();
			grid_data[row][col - 1].activateEnemy();
		}
		
		private function addBoardToStage():void
		{
			var gridPiece:Piece;
			for (var i:int = 0; i < grid_data.length; i++) 
			{
				for (var j:int = 0; j < grid_data[i].length; j++) 
				{
					gridPiece = grid_data[i][j];
					addChild(gridPiece);
					gridPiece.x  = gridPiece.pos_grid.x * (gridPiece.width + RANGE)  ;
					gridPiece.y  = gridPiece.pos_grid.y * (gridPiece.height + RANGE) ;
				}
			}
		}
		
		private function addListeners():void
		{
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function onResize(e:Event = null):void
		{
			this.x = MainApplication.instance.stage.stageWidth>>1;
			this.y = MainApplication.instance.stage.stageHeight>>1;
			background.x = -11;
			background.y = -10;
		}
		
		private function destroy(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			stage.removeEventListener(Event.RESIZE, onResize);
		}
		private function console():void
		{
			var data:String = "";
			for (var i:int = 0; i < 8; i++) 
			{
				for (var j:int = 0; j < 8; j++) 
				{
					if(grid_data[j][i] != null) data += grid_data[j][i].active + " ";
					else data += ". ";
					
					//					if(grid_data[j][i] != null) data += grid_data[j][i].pos_grid.toString() + " ";
					//					else data += ". ";
				}
				data += "\n";
			}
			trace(data);
		}
	}
}