package GameLogic.Controllers_$_Views.InGame
{
	import com.greensock.TweenMax;
	
	import Events.GameModelEvents;
	
	import GameLogic.Models.EnemyBoardModel;
	import GameLogic.Models.PlayerBoardModel;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class InfoGame extends Sprite
	{
		static private var _init:InfoGame;
		static public function instance():InfoGame { return _init; }
		private var _playerBoardModel:PlayerBoardModel;
		private var _enemyBoardModel:EnemyBoardModel;
		private var __playerInfoTF:TextField;
		private var __enemyInfoTF:TextField;
		public var __enemyNoMove:Boolean = false;
		public var __playerNoMove:Boolean = false;
		public function InfoGame()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			_init = this;
			_playerBoardModel = PlayerBoardModel.INSTANCE;
			_enemyBoardModel = EnemyBoardModel.INSTANCE;
			
			createInfoElements();
			
			addListeners();
			onResize();
		}
		
		private function createInfoElements():void
		{
			__playerInfoTF = new TextField(300, 400, "У врага нет возможных ходов  - переход хода для игрока",  MAssetsManager.CAMPUS, 27, 0xFFB331);
			__playerInfoTF.alignPivot();
			__playerInfoTF.touchable = false;
			addChild(__playerInfoTF);
			__playerInfoTF.alpha = 0;
			
			__enemyInfoTF = new TextField(300, 400, "У Вас нет возможных ходов - переход хода для врага",  MAssetsManager.CAMPUS, 27, 0xFFB331);
			__enemyInfoTF.alignPivot();
			__enemyInfoTF.touchable = false;
			addChild(__enemyInfoTF);
			__enemyInfoTF.alpha = 0;
		}
		
		private function addListeners():void
		{
			stage.addEventListener(Event.RESIZE, onResize);
			_playerBoardModel.addEventListener(GameModelEvents.PLAYER_NO_MOVE, animatePlayerInfo);
			_enemyBoardModel.addEventListener(GameModelEvents.ENEMY_NO_MOVE, animateEnemyInfo);
		}
		
		private function animateEnemyInfo(e:Event):void
		{
			__enemyNoMove = true;
			
			if(__playerNoMove &&  __enemyNoMove )
			{
				GameStatPanel.instance().endGame();
				return;
			}
			
			if(__enemyInfoTF.alpha != 0 ) 
			{
				TweenMax.to(__enemyInfoTF, .1, { alpha:0});
			}
			TweenMax.to(__playerInfoTF, 2, { alpha:1, onComplete:CloseInfo});
			 function CloseInfo():void
			{
				 TweenMax.to(__playerInfoTF, 2, { alpha:0});
			}
		}
		
		private function animatePlayerInfo(e:Event):void
		{
			__playerNoMove = true;
			
			if(__playerNoMove &&  __enemyNoMove )
			{
				GameStatPanel.instance().endGame();
				return;
			}
			
			if(__playerInfoTF.alpha != 0 ) 
			{
				TweenMax.to(__playerInfoTF, .1, { alpha:0});
			}
			TweenMax.to(__enemyInfoTF, 2, { alpha:1, onComplete:CloseInfo});
			function CloseInfo():void
			{
				TweenMax.to(__enemyInfoTF, 2, { alpha:0});
			}
		}
		
		private function onResize(e:Event = null):void
		{
			this.x = MainApplication.instance.stage.stageWidth>>1;
			this.y = MainApplication.instance.stage.stageHeight - 45;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			stage.removeEventListener(Event.RESIZE, onResize);
			_playerBoardModel.removeEventListener(GameModelEvents.PLAYER_NO_MOVE, animatePlayerInfo);
			_enemyBoardModel.removeEventListener(GameModelEvents.ENEMY_NO_MOVE, animateEnemyInfo);
		}
	}
}