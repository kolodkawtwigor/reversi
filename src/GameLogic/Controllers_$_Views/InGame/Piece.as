package GameLogic.Controllers_$_Views.InGame
{
	
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import GameLogic.MapScreen;
	import GameLogic.GameHelpingService.MImage;
	import GameLogic.Models.PlayerBoardModel;
	
	import starling.display.Canvas;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;

	public class Piece extends Sprite
	{
		private var __pos_grid:Point;
		public  const PIECE_WIDTH:Number = Service.instance.configurationModel.pieceWidth;
		public  const PIECE_HEIGHT:Number = Service.instance.configurationModel.pieceWidth;
		public  const PIECE_COLOR:Number = Service.instance.configurationModel.pieceColor;
		
		
//		public  const  WHITE_COLOR:Number = MapScreen.PLAYER_COLOR;
//		public  const  BLACK_COLOR:Number = MapScreen.ENEMY_COLOR;
		public  const  WHITE_COLOR:String = MapScreen.PLAYER_COLOR;
		public  const  BLACK_COLOR:String = MapScreen.ENEMY_COLOR;
		
		
//		private var __whiteElement:Canvas;
//		private var __blackElement:Canvas;
		private var __whiteElement:MImage;
		private var __blackElement:MImage;
		private var __active:Boolean;
		private var __enemyPiece:Boolean = false;;
		private var __playerPiece:Boolean = false;
		
		public function Piece()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			createPiece();
		}
		
		private function createPiece():void
		{
			createSimpleGraphics();
			createGameElements();
			
			addEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
		
		private function createGameElements():void
		{
			var texture:Texture = MAssetsManager.assetManager.getTexture(WHITE_COLOR);
			playerElement = new MImage(texture); 
			playerElement.x = this.width>>1 ;
			playerElement.y = this.height>>1;
			playerElement.width = 45;
			playerElement.height = 45;
			playerElement.alignPivot();
			addChild(playerElement);
			playerElement.alpha = 0;
			
			texture = MAssetsManager.assetManager.getTexture(BLACK_COLOR);
			enemyElement = new MImage(texture); 
			enemyElement.x = this.width>>1 ;
			enemyElement.y = this.height>>1;
			enemyElement.width = 45;
			enemyElement.height = 45;
			enemyElement.alignPivot();
			addChild(enemyElement);
			enemyElement.alpha = 0;
		}
		
		private function createSimpleGraphics():void
		{
			var __rect:Canvas = new Canvas();
			__rect.beginFill(PIECE_COLOR);
			__rect.drawRectangle(0,0,PIECE_WIDTH,PIECE_HEIGHT);
			addChild(__rect)
		}
		
		protected function onGameKeyClick(e:TouchEvent):void
		{
//			trace(pos_grid)
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			if( touches.length == 0) 
			{
				if(!active) 
				{
					playerElement.alpha = 0;
					enemyElement.alpha = 0;
				}
			}
			if(active)  return;
			
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					if(!active) playerElement.alpha = 0;
				}
				
				if (touch.phase == TouchPhase.BEGAN)
				{
					PlayerBoardModel.INSTANCE.checkPiece(this);
				}
				
				if (touch.phase == TouchPhase.HOVER)
				{
					playerElement.alpha = .4;
				}
			}
		}
		
		public function  deActivateEnemy():void
		{
			enemyPiece = false;
			active = true;
			playerPiece = true;
			
			TweenMax.to(enemyElement, .2, { scaleX:0, onComplete:completeEnemyDeactive});
			 function completeEnemyDeactive():void
			{
				 enemyElement.alpha = 0;
				 enemyElement.scaleX = .35;
				 
				 playerElement.alpha = 1;
				 playerElement.scaleX = 0;
				 TweenMax.to(playerElement, .2, { scaleX:.35});
			}
		}
		
		public function  deActivatePlayer():void
		{
//			playerElement.alpha = 0;
			playerPiece = false;
			
//			enemyElement.alpha = 1;
			enemyPiece = true;
			active = true;
			
			
			TweenMax.to(playerElement, .2, { scaleX:0, onComplete:completePlayerDeactive});
			function completePlayerDeactive():void
			{
				playerElement.alpha = 0;
				playerElement.scaleX = .35;
				
				enemyElement.alpha = 1;
				enemyElement.scaleX = 0;
				TweenMax.to(enemyElement, .2, { scaleX:.35});
			}
			
			
			
			
		}
		
		public function  activatePlayer():void
		{
			playerElement.alpha = 1;
			active = true;
			playerPiece = true;
		}
		
		public function  activateEnemy():void
		{
			enemyElement.alpha = 1;
			enemyPiece = true;
			active = true;
		}
		
		
		public function get playerPiece():Boolean
		{
			return __playerPiece;
		}
		
		public function set playerPiece(value:Boolean):void
		{
			__playerPiece = value;
		}
		
		public function get enemyPiece():Boolean
		{
			return __enemyPiece;
		}
		
		public function set enemyPiece(value:Boolean):void
		{
			__enemyPiece = value;
		}
		
		public function get active():Boolean
		{
			return __active;
		}
		
		public function set active(value:Boolean):void
		{
			__active = value;
		}
		
		public function get pos_grid():Point
		{
			return __pos_grid;
		}
		
		public function set pos_grid(value:Point):void
		{
			__pos_grid = value;
		}
		
		public function get enemyElement():MImage
		{
			return __blackElement;
		}
		
		public function set enemyElement(value:MImage):void
		{
			__blackElement = value;
		}
		
		public function get playerElement():MImage
		{
			return __whiteElement;
		}
		
		public function set playerElement(value:MImage):void
		{
			__whiteElement = value;
		}
//		public function get enemyElement():Canvas
//		{
//			return __blackElement;
//		}
//		
//		public function set enemyElement(value:Canvas):void
//		{
//			__blackElement = value;
//		}
//		
//		public function get playerElement():Canvas
//		{
//			return __whiteElement;
//		}
//		
//		public function set playerElement(value:Canvas):void
//		{
//			__whiteElement = value;
//		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			removeEventListener(TouchEvent.TOUCH , onGameKeyClick);
		}
	}
}