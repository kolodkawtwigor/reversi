package GameLogic
{
	import com.greensock.TweenMax;
	
	import flash.text.Font;
	import flash.text.TextFormatAlign;
	
	import GameLogic.GameHelpingService.MImage;
	import GameLogic.GameHelpingService.WindowsManager;
	
	import Windows.ChooseSide;
	import Windows.Rule;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	import Windows.ChooseComplexity;

	public class MapScreen extends Sprite
	{
		private var __startTF:TextField;
		public static var PLAYER_COLOR:String;
		public static var ENEMY_COLOR:String;
		private var _background:MImage;
		private var __ruleTF:TextField;
		private var __firstGameName:TextField;
		private var __secondGameName:TextField;
		
		public function MapScreen()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createMapElements();
			animateElements();
			addListeners();
			
		}
		
		private function animateElements():void
		{
			var animateFirstNameX:int = (MainApplication.instance.stage.stageWidth>>1) -90;
			var animateSecondNameX:int = (MainApplication.instance.stage.stageWidth>>1) +100;
			
			TweenMax.to(__firstGameName, 1, { x:animateFirstNameX, onComplete:animateStartTF});
			TweenMax.to(__secondGameName, 1, { x:animateSecondNameX, onComplete:animateRuleTF});
			 function animateStartTF():void
			{
				 var animateY:int = MainApplication.instance.stage.stageHeight>>1;
				 TweenMax.to(__startTF, 2, { y:animateY});
			}
			 function animateRuleTF():void
			{
				 var animateY:int = (MainApplication.instance.stage.stageHeight>>1) + 100;
				 TweenMax.to(__ruleTF, 2, { y:animateY});
			}
		}
		
		private function createMapElements():void
		{
			var texture:Texture = MAssetsManager.assetManager.getTexture("bg_game_screen");
			_background = new MImage(texture); 
			addChild(_background);
			_background.width =  MainApplication.instance.stage.stageWidth
			_background.height =  MainApplication.instance.stage.stageHeight
			
			__startTF = new TextField(200, 50, "START GAME", MAssetsManager.ARISTA, 27, 0x362F27);
			__startTF.alignPivot();
			__startTF.useHandCursor = true; 
			__startTF.hAlign = TextFormatAlign.CENTER;  
			__startTF.batchable = true;
			addChild(__startTF);  
			
			__ruleTF = new TextField(200, 50, "Правила игры", MAssetsManager.CAMPUS, 27, 0x362F27);
			__ruleTF.alignPivot();
			__ruleTF.useHandCursor = true; 
			__ruleTF.hAlign = TextFormatAlign.CENTER;  
			__ruleTF.batchable = true;
			addChild(__ruleTF);  
			
			__firstGameName = new TextField(300, 200, "REV", MAssetsManager.CAMPUS, 100, 0x362F27);
			__firstGameName.alignPivot();
			__firstGameName.useHandCursor = true; 
			__firstGameName.hAlign = TextFormatAlign.CENTER;
			__firstGameName.batchable = true;
			addChild(__firstGameName);
			
			__secondGameName = new TextField(300, 200, "ERSI", MAssetsManager.CAMPUS, 100, 0x362F27);
			__secondGameName.alignPivot();
			__secondGameName.useHandCursor = true; 
			__secondGameName.hAlign = TextFormatAlign.CENTER;  
			__secondGameName.batchable = true;
			addChild(__secondGameName);  
			
			__startTF.x = MainApplication.instance.stage.stageWidth>>1;
			__startTF.y = MainApplication.instance.stage.stageHeight + 100;
			
			__ruleTF.x = __startTF.x;
			__ruleTF.y = __startTF.y + 200;
			
			__firstGameName.x = -200;
			__firstGameName.y = 200;
			__secondGameName.x = MainApplication.instance.stage.stageWidth + 200;
			__secondGameName.y = 200;
		}
		
		private function addListeners():void
		{
			stage.addEventListener(Event.RESIZE, onResize);
			__startTF.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
			__ruleTF.addEventListener(TouchEvent.TOUCH , onGameKeyClickRule);
		}
		
		protected function onGameKeyClickRule(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.BEGAN) WindowsManager.show( Rule );
			}
		}
		
		protected function onGameKeyClick(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.BEGAN) WindowsManager.show( ChooseSide );
			}
		}
		
		private function onResize(e:Event = null):void
		{
			__startTF.x = MainApplication.instance.stage.stageWidth>>1;
			__startTF.y = MainApplication.instance.stage.stageHeight>>1;
			
			__ruleTF.x = __startTF.x;
			__ruleTF.y = (MainApplication.instance.stage.stageHeight>>1) + 100;
			
			__firstGameName.x = (MainApplication.instance.stage.stageWidth>>1) -90;
			__firstGameName.y = 200;
			__secondGameName.x = (MainApplication.instance.stage.stageWidth>>1) +100;
			__secondGameName.y = 200;
			
			_background.width =  MainApplication.instance.stage.stageWidth
			_background.height =  MainApplication.instance.stage.stageHeight
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			stage.removeEventListener(Event.RESIZE, onResize);
		}
	}
}