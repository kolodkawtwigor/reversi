package
{
	import starling.events.EventDispatcher;
	
	public class ConfigurationModel extends EventDispatcher
	{
		private var __pieceWidth:uint;
		private var __pieceColor:uint;
		private var __playerColorElement:String;
		private var __enemyColorElement:String;
		private var __pieceRange:uint;
		private var __boardRow:uint;
		private var __boardCol:uint;
		private var __backgroundColor:uint;
		public function ConfigurationModel()
		{
			super();
		}

		public function get backgroundColor():uint
		{
			return __backgroundColor;
		}

		public function set backgroundColor(value:uint):void
		{
			__backgroundColor = value;
		}

		public function get boardCol():uint
		{
			return __boardCol;
		}

		public function set boardCol(value:uint):void
		{
			__boardCol = value;
		}

		public function get boardRow():uint
		{
			return __boardRow;
		}

		public function set boardRow(value:uint):void
		{
			__boardRow = value;
		}

		public function get pieceRange():uint
		{
			return __pieceRange;
		}

		public function set pieceRange(value:uint):void
		{
			__pieceRange = value;
		}

		public function get enemyColorElement():String
		{
			return __enemyColorElement;
		}

		public function set enemyColorElement(value:String):void
		{
			__enemyColorElement = value;
		}

		public function get playerColorElement():String
		{
			return __playerColorElement;
		}

		public function set playerColorElement(value:String):void
		{
			__playerColorElement = value;
		}

		public function get pieceColor():uint
		{
			return __pieceColor;
		}

		public function set pieceColor(value:uint):void
		{
			__pieceColor = value;
		}

		public function get pieceWidth():uint
		{
			return __pieceWidth;
		}

		public function set pieceWidth(value:uint):void
		{
			__pieceWidth = value;
		}

	}
}