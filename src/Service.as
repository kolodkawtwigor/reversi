package
{
	public class Service
	{
		static private var _init:Service;
		static public function get instance():Service
		{
			if(!_init) _init = new Service();
			return _init; 
		}
		private var __configurationModel			:ConfigurationModel;
		public function Service()
		{
		}
		
		public function get configurationModel():ConfigurationModel
		{
			if(!__configurationModel) __configurationModel = new ConfigurationModel();
			return __configurationModel;
		}
		
		public function set configurationModel(value:ConfigurationModel):void
		{
			__configurationModel = value;
		}
	}
}