package
{
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	public class ConfigurationController
	{
		private var __callBack:Function;
		public function ConfigurationController(initGame:Function)
		{
			__callBack = initGame;
			super();
			downloadConfigurationXML();
		}
		
		private function downloadConfigurationXML():void
		{
			var loader:URLLoader = new URLLoader();
			var rand:uint = Math.round(Math.random()*uint.MAX_VALUE);
			var request:URLRequest = new URLRequest("../media/Configuration.xml?" + rand);
			loader.load(request);
			loader.addEventListener(Event.COMPLETE,onComplete);
		}
		
		private function onComplete(event:Event):void
		{
			var configurationModel:ConfigurationModel  = Service.instance.configurationModel;
			
			var myxml:XML = XML(event.target.data);
			
			configurationModel.pieceWidth 			= parseInt(myxml.pieceWidth.text(),10);;
			configurationModel.pieceColor 			= parseInt(myxml.pieceColor.text(), 16);
			configurationModel.playerColorElement 	= myxml.playerColorElement.text();
			configurationModel.enemyColorElement 	= myxml.enemyColorElement.text();
			configurationModel.pieceRange		    = parseInt(myxml.pieceRange.text(),10);
			configurationModel.boardRow			    = parseInt(myxml.boardRow.text(),10);
			configurationModel.boardCol 			= parseInt(myxml.boardCol.text(),10);
			configurationModel.backgroundColor		= parseInt(myxml.backgroundColor.text(),16);
			
			__callBack.call();
		}
		
	}
}