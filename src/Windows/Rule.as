package Windows
{
	import flash.text.TextFormatAlign;
	
	import GameLogic.GameScreen;
	import GameLogic.GameHelpingService.MImage;
	import GameLogic.GameHelpingService.ScreenManager;
	import GameLogic.GameHelpingService.WindowsManager;
	
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class Rule extends Sprite
	{
		private var _background:MImage;
		private var _nextBTN:Button;
		private var _closetBTN:Button;
		private var __ruleTF:TextField;
		public function Rule()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createElements();
			createTF();
			
			onResize();
			addListeners()
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function createTF():void
		{
			var text:String = "Делая ход, игрок должен поставить свою фишку на \n" +
				"одну из клеток доски таким образом, чтобы между этой поставленной \n" +
				"фишкой и одной из имеющихся уже на доске фишек его цвета находился \n" +
				"непрерывный ряд фишек соперника, горизонтальный, \n" +
				"вертикальный или диагональный (другими словами, чтобы непрерывный ряд \n" +
				"фишек соперника оказался «закрыт» фишками игрока с двух сторон). Все фишки \n" +
				"соперника, входящие в «закрытый» на этом ходу ряд, переворачиваются на другую \n" +
				"сторону (меняют цвет) и переходят к ходившему игроку."
			__ruleTF = new TextField(600, 500, text, MAssetsManager.CAMPUS, 22, 0x362F27);
			__ruleTF.alignPivot();
			__ruleTF.hAlign = TextFormatAlign.CENTER;  
			__ruleTF.touchable = false;
			addChild(__ruleTF); 
		}
		private function addListeners():void
		{
			stage.addEventListener(Event.RESIZE, onResize);
			_nextBTN.addEventListener(TouchEvent.TOUCH , nextWindow);
			_closetBTN.addEventListener(TouchEvent.TOUCH , closeWindow);
		}
		
		protected function closeWindow(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED) WindowsManager.hide();
			}
		}
		protected function nextWindow(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED) 
				{
					WindowsManager.hide(); 
					WindowsManager.show( Rule2);
				}
			}
		}
		
		private function createElements():void
		{
			var texture:Texture = MAssetsManager.assetManager.getTexture("help_window");
			_background = new MImage(texture); 
			addChild(_background);
			
			_nextBTN = new Button(MAssetsManager.assetManager.getTexture("next_BTN"))
			_nextBTN.scaleWhenDown = .9;
			addChild(_nextBTN);
			_nextBTN.alignPivot();
			
			_closetBTN = new Button(MAssetsManager.assetManager.getTexture("close_BTN"))
			_closetBTN.scaleWhenDown = .9;
			addChild(_closetBTN);
			_closetBTN.alignPivot();
		}
		
		private function onResize(e:Event = null):void
		{
			
			_nextBTN.x = 300;
			_nextBTN.y = 550;
			
			_closetBTN.x = 610;
			_closetBTN.y = 160;
//			
			__ruleTF.x = (this.width>>1) -150
			__ruleTF.y = (this.height>>1) -100
//			__ruleTF.x = 350
//			__ruleTF.y = 300
				
			this.x = MainApplication.instance.stage.stageWidth>>1;
			this.y = MainApplication.instance.stage.stageHeight>>1;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}