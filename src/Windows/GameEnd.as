package Windows
{
	import flash.text.Font;
	import flash.text.TextFormatAlign;
	
	import GameLogic.GameScreen;
	import GameLogic.MapScreen;
	import GameLogic.Controllers_$_Views.InGame.GameStatPanel;
	import GameLogic.GameHelpingService.MImage;
	import GameLogic.GameHelpingService.ScreenManager;
	import GameLogic.GameHelpingService.WindowsManager;
	
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class GameEnd extends Sprite
	{
		private var _background:MImage;
		private var __resultTF:TextField;
		private var __result:String = GameStatPanel.instance().gameResult;
		private var _closetBTN:Button;
		private var __outplay:Button;
		private var __toMap:Button;
		public function GameEnd()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createBG();
			createTF();
			createBTN();
			
			onResize();
			addListeners();
		}
		
		private function addListeners():void
		{
			stage.addEventListener(Event.RESIZE, onResize);
			__outplay.addEventListener(TouchEvent.TOUCH , onGameKeyClick);
			__toMap.addEventListener(TouchEvent.TOUCH , onGameKeyClickToMap);
			_closetBTN.addEventListener(TouchEvent.TOUCH , closeWindow);
		}
		protected function closeWindow(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED) WindowsManager.hide();
			}
		}
		
		protected function onGameKeyClickToMap(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED) 
				{
					WindowsManager.hide();
					ScreenManager.instance.show( MapScreen );
				}
			}
		}
		
		protected function onGameKeyClick(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED) 
				{
					WindowsManager.hide();
					ScreenManager.instance.show( GameScreen );
				}
			}
		}
		
		private function createBTN():void
		{
			__outplay = new Button(MAssetsManager.assetManager.getTexture("reGame_button"))
			__outplay.scaleWhenDown = .9;
			addChild(__outplay);
			__outplay.alignPivot();
			__toMap = new Button(MAssetsManager.assetManager.getTexture("toMap_main_BTN"))
			__toMap.scaleWhenDown = .9;
			addChild(__toMap);
			__toMap.alignPivot();
			
			_closetBTN = new Button(MAssetsManager.assetManager.getTexture("close_BTN"))
			_closetBTN.scaleWhenDown = .9;
			addChild(_closetBTN);
			_closetBTN.alignPivot();
			
			__outplay.x = 150;
			__outplay.y = 300;
			__toMap.x = 430;
			__toMap.y = 300;
			_closetBTN.x = 550;
			_closetBTN.y = 50;
			
		}
		
		private function createTF():void
		{
			var fonts:Array = Font.enumerateFonts(true);
			
			
			var text:String;
			
			switch(__result)
			{
				case "playerWin":
				{
					text = "ВЫ ВЫИГРАЛИ!!!"
					break;
				}
				case "enemyWin":
				{
					text = "ВЫ ПРОИГРАЛИ =("
					break;
				}
				case "draw":
				{
					text = "ПОБЕДИЛА ДРУЖБА"
					break;
				}
				case "noMoveWinPlayer":
				{
					text = "Закончились ходы у обоих игроков \n ВЫ ВЫИГРАЛИ!!!"
					break;
				}
				case "noMoveWinEnemy":
				{
					text = "Закончились ходы у обоих игроков \n ВЫ ПРОИГРАЛИ =("
					break;
				}
				case "noMoveDraw":
				{
					text = "Закончились ходы у обоих игроков \n ПОБЕДИЛА ДРУЖБА"
					break;
				}
			}
			
			__resultTF = new TextField(300, 100, text, MAssetsManager.CAMPUS, 27, 0x362F27);
			__resultTF.alignPivot();
			__resultTF.hAlign = TextFormatAlign.CENTER;
			__resultTF.batchable = true;
			__resultTF.x = this.width>>1;
			__resultTF.y = 130;
			addChild(__resultTF); 
		}
		
		private function createBG():void
		{
			var texture:Texture = MAssetsManager.assetManager.getTexture("secondMain_WIN");
			_background = new MImage(texture); 
			addChild(_background);
		}
		
		
		private function onResize(e:Event = null):void
		{
			this.x = MainApplication.instance.stage.stageWidth>>1;
			this.y = MainApplication.instance.stage.stageHeight>>1;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);
			stage.removeEventListener(Event.RESIZE, onResize);
			__outplay.removeEventListener(TouchEvent.TOUCH , onGameKeyClick);
			__toMap.removeEventListener(TouchEvent.TOUCH , onGameKeyClickToMap);
			_closetBTN.removeEventListener(TouchEvent.TOUCH , closeWindow);
		}
	}
}