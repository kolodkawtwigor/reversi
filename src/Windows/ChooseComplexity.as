package Windows
{
	
	import flash.text.TextFormatAlign;
	
	import GameLogic.GameScreen;
	import GameLogic.MapScreen;
	import GameLogic.GameHelpingService.MImage;
	import GameLogic.GameHelpingService.ScreenManager;
	import GameLogic.GameHelpingService.WindowsManager;
	import GameLogic.Models.GameModel;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class ChooseComplexity extends Sprite
	{
		private var _background:MImage;
		private var __complexityTF:TextField;
		private var __complexityHARD:TextField;
		private var __complexityEASY:TextField;
		private var __complexityMID:TextField;
		public function ChooseComplexity()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createBG();
			createTF();
			createBNT();
			onResize();
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function createBNT():void
		{
			__complexityHARD = new TextField(150, 50, "СЛОЖНЫЙ", MAssetsManager.CAMPUS, 27, 0xF5980A);
			__complexityHARD.alignPivot();
			__complexityHARD.hAlign = TextFormatAlign.CENTER;  
			__complexityHARD.batchable = true;
			__complexityHARD.x = (this.width>>1) + 150 
			__complexityHARD.y = 200;
			__complexityHARD.useHandCursor = true;
			addChild(__complexityHARD); 
			__complexityHARD.addEventListener(TouchEvent.TOUCH , chooseHard);
			
			__complexityMID = new TextField(150, 50, "СРЕДНИЙ", MAssetsManager.CAMPUS, 27, 0xAD6FF9);
			__complexityMID.alignPivot();
			__complexityMID.hAlign = TextFormatAlign.CENTER;  
			__complexityMID.batchable = true;
			__complexityMID.x = (this.width>>1);
			__complexityMID.y = 200;
			__complexityMID.useHandCursor = true;
			addChild(__complexityMID); 
			__complexityMID.addEventListener(TouchEvent.TOUCH , chooseMid);
			
			__complexityEASY = new TextField(150, 50, "ЛЕГКИЙ", MAssetsManager.CAMPUS, 27, 0xBCE2BD);
			__complexityEASY.alignPivot();
			__complexityEASY.hAlign = TextFormatAlign.CENTER;  
			__complexityEASY.batchable = true;
			__complexityEASY.x = (this.width>>1) - 150;
			__complexityEASY.y = 200;
			__complexityEASY.useHandCursor = true;
			addChild(__complexityEASY); 
			__complexityEASY.addEventListener(TouchEvent.TOUCH , chooseEasy);
			
			
//			ScreenManager.instance.show( GameScreen );
		}
		
		private function chooseHard(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					WindowsManager.hide();
					ScreenManager.instance.show( GameScreen );
					GameModel.INSTANCE.complexity = "HARD";
				}
			}
		}
		private function chooseMid(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					WindowsManager.hide();
					ScreenManager.instance.show( GameScreen );
					GameModel.INSTANCE.complexity = "MID";
				}
			}
		}
		private function chooseEasy(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					WindowsManager.hide();
					ScreenManager.instance.show( GameScreen );
					GameModel.INSTANCE.complexity = "EASY";
				}
			}
		}
		
		private function createTF():void
		{
			__complexityTF = new TextField(300, 100, "Выберите сложность", MAssetsManager.CAMPUS, 27, 0x362F27);
			__complexityTF.alignPivot();
			__complexityTF.hAlign = TextFormatAlign.CENTER;  
			__complexityTF.batchable = true;
			__complexityTF.x = this.width>>1
			__complexityTF.y = 100;
			addChild(__complexityTF);  
		}
		
		
		private function createBG():void
		{
			var texture:Texture = MAssetsManager.assetManager.getTexture("secondMain_WIN");
			_background = new MImage(texture); 
			addChild(_background);
		}
		
		private function onResize(e:Event = null):void
		{
			this.x = MainApplication.instance.stage.stageWidth>>1;
			this.y = MainApplication.instance.stage.stageHeight>>1;
		}
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}