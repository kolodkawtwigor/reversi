package Windows
{
	import flash.text.TextFormatAlign;
	
	import GameLogic.GameScreen;
	import GameLogic.GameHelpingService.MImage;
	import GameLogic.GameHelpingService.ScreenManager;
	import GameLogic.GameHelpingService.WindowsManager;
	
	import starling.display.Button;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class Rule2 extends Sprite
	{
		private var _background:MImage;
		private var _nextBTN:Button;
		private var _closetBTN:Button;
		private var __ruleTF:TextField;
		public function Rule2()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createElements();
			createTF();
			
			onResize();
			addListeners()
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function createTF():void
		{
			var text:String = "Если в результате одного хода «закрывается» одновременно более одного ряда \n" +
				"фишек противника, то переворачиваются все фишки, оказавшиеся на всех «закрытых» рядах.\n" +
				"Игрок вправе выбирать любой из возможных для него ходов. \n" +
				"Если игрок имеет возможные ходы, он не может отказаться от хода. \n" +
				"Если игрок не имеет допустимых ходов, то ход \n передаётся сопернику."

			__ruleTF = new TextField(600, 500, text, MAssetsManager.CAMPUS, 22, 0x362F27);
			__ruleTF.alignPivot();
			__ruleTF.hAlign = TextFormatAlign.CENTER;  
			__ruleTF.touchable = false;
			addChild(__ruleTF); 
		}
		private function addListeners():void
		{
			stage.addEventListener(Event.RESIZE, onResize);
			_closetBTN.addEventListener(TouchEvent.TOUCH , closeWindow);
		}
		
		protected function closeWindow(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED) WindowsManager.hide();
			}
		}
		
		
		private function createElements():void
		{
			var texture:Texture = MAssetsManager.assetManager.getTexture("help_window");
			_background = new MImage(texture); 
			addChild(_background);
			
			
			_closetBTN = new Button(MAssetsManager.assetManager.getTexture("close_BTN"))
			_closetBTN.scaleWhenDown = .9;
			addChild(_closetBTN);
			_closetBTN.alignPivot();
		}
		
		private function onResize(e:Event = null):void
		{
			
			_closetBTN.x = 610;
			_closetBTN.y = 160;
			
			__ruleTF.x = (this.width>>1) -150
			__ruleTF.y = (this.height>>1) -100
			
			this.x = MainApplication.instance.stage.stageWidth>>1;
			this.y = MainApplication.instance.stage.stageHeight>>1;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}