package Windows
{
	import flash.text.Font;
	import flash.text.TextFormatAlign;
	
	import GameLogic.GameScreen;
	import GameLogic.MapScreen;
	import GameLogic.GameHelpingService.MImage;
	import GameLogic.GameHelpingService.ScreenManager;
	import GameLogic.GameHelpingService.WindowsManager;
	
	import starling.display.Button;
	import starling.display.Canvas;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class ChooseSide extends Sprite
	{
		private var __chooseTF:TextField;
		private var firstElement:Button;
		private var secondElement:Button;
		public  const  WHITE_COLOR:String = Service.instance.configurationModel.playerColorElement;
		public  const  BLACK_COLOR:String = Service.instance.configurationModel.enemyColorElement;
		private var _background:MImage;
		
		public function ChooseSide()
		{
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			createBG();
			createTF();
			createChoosingElements();
			onResize();
			stage.addEventListener(Event.RESIZE, onResize);
		}
		
		private function createChoosingElements():void
		{
			var texture:Texture = MAssetsManager.assetManager.getTexture("done_lvl_simple_BTN");
			firstElement = new Button(texture); 
			firstElement.x = (this.width>>1) - 100 ;
			firstElement.y = this.height>>1 ;
			firstElement.alignPivot();
			firstElement.touchable = true;
			addChild(firstElement);
			
			
			
			
			firstElement.addEventListener(TouchEvent.TOUCH , chooseFirstElement);
			
			texture = MAssetsManager.assetManager.getTexture("close_lvl_simple_BTN");
			secondElement = new Button(texture); 
			secondElement.x = (this.width>>1) + 100;
			secondElement.y = this.height>>1 ;
			secondElement.alignPivot();
			secondElement.touchable = true;
			addChild(secondElement);
			secondElement.addEventListener(TouchEvent.TOUCH , chooseSecondElement);
		}
		
		private function chooseSecondElement(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED)
				{
					WindowsManager.hide();
					MapScreen.PLAYER_COLOR = BLACK_COLOR;
					MapScreen.ENEMY_COLOR = WHITE_COLOR;
					WindowsManager.show(  ChooseComplexity );
				}
			}
		}
		
		private function chooseFirstElement(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			for each (var touch:Touch in touches) 
			{
				if (touch.phase == TouchPhase.ENDED) 
				{
					WindowsManager.hide();
					MapScreen.PLAYER_COLOR = WHITE_COLOR;
					MapScreen.ENEMY_COLOR = BLACK_COLOR;
					WindowsManager.show(  ChooseComplexity );
				}
			}
		}
		
		private function createTF():void
		{
			__chooseTF = new TextField(300, 100, "Выберите фишку", MAssetsManager.CAMPUS, 27, 0x362F27);
			__chooseTF.alignPivot();
			__chooseTF.hAlign = TextFormatAlign.CENTER;  
			__chooseTF.batchable = true;
			__chooseTF.x = this.width>>1
			__chooseTF.y = 100;
			addChild(__chooseTF);  
		}
		
		private function createBG():void
		{
			var texture:Texture = MAssetsManager.assetManager.getTexture("secondMain_WIN");
			_background = new MImage(texture); 
			addChild(_background);
		}
		
		private function onResize(e:Event = null):void
		{
			this.x = MainApplication.instance.stage.stageWidth>>1;
			this.y = MainApplication.instance.stage.stageHeight>>1;
		}
		
		private function destroy(event:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			firstElement.removeEventListener(TouchEvent.TOUCH , chooseFirstElement);
			stage.removeEventListener(Event.RESIZE, onResize);
			secondElement.removeEventListener(TouchEvent.TOUCH , chooseSecondElement);
		}
	}
}