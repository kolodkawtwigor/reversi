package
{
	import flash.system.Security;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class MainApplication extends Sprite
	{
		public static var BLOCK:int;
		private var __screenHolst:Sprite = new Sprite();
		static private var _init:MainApplication;
		private var _config:ConfigurationController;
		private var _mAssetsManager:MAssetsManager;
		static public function get instance():MainApplication { return _init; }
		
		public function MainApplication()
		{
			_init = this
			super();
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			addEventListener(Event.REMOVED_FROM_STAGE, destroy);
		}

		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			Security.allowDomain('*');
			addChild(__screenHolst);
			
			initServer();
		}
		
		private function initServer():void
		{
			_config = new ConfigurationController(initAssets);
		}
		
		private function initAssets():void
		{
			_mAssetsManager = new MAssetsManager(initGame);
		}
		
		private function initGame():void
		{
			GameInit.instance.start();
		}
		
		private function destroy(e:Event):void
		{
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}